# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


"""Utilities for whining matrix dashboard.

Utilities shared by both models and backend.

"""

import datetime
import logging
import math
import re
import time

from src import whining_errors


EPOCH = datetime.datetime(1970, 1, 1)
NATSORT_REGEXP = re.compile(r'(\d+|\D+)')
NUMBER_REGEXP = re.compile('(\d+)|(0x[0-1A-Fa-f]+)')

REASON_NORMALIZATION_LENGTH = 200

BUG_URL_BASE = '//code.google.com/p/chromium/issues'

ANDROID_DEFAULT_REV = 0
# A regexp for buildname like R23-2913.327.0-rc1-b456
cros_re_build = re.compile('R(\d+)-(\d+)\.(\d+)\.(\d+)[-]?(.*)')
android_re_build = re.compile(r'[0-9]+')

def build2sortable(buildname):
    """Make a buildname string well sortable by prefixing numbers with zeros.
    """
    return natsort_add_zeros(buildname, 5)


def cros_build2tuple(buildname):
    """Given a cros build name match convert it to a tuple of components.

    Examples:
    R23-2913.327.0-rc1-b456 -> (23, 2913, 327, 0, 'rc1-b456')
    R23-2913.327.0          -> (23, 2913, 327, 0, '')
    """
    m = cros_re_build.match(buildname)
    if not m:
        return None
    t = m.groups()
    #the last part (rcXX-bXXX) styas as a string. In most cases it's empty.
    lst = [int(x) for x in t[0:-1]]
    lst.append(t[-1])
    return tuple(lst)


def android_build2_tuple(buildname):
    """Given an android build name match convert it to a tuple of components.

    Examples:
    2660887 -> (0, 2660887, 0, 0, '')
    """
    m = android_re_build.match(buildname)
    if not m:
        return None
    lst = [ANDROID_DEFAULT_REV, buildname.split('/')[-1], 0, 0, '']
    return tuple(lst)

def build2tuple(buildname):
    """Given a build name convert it to a tuple of components.

    """
    result = cros_build2tuple(buildname)
    if not result:
        result = android_build2_tuple(buildname)
    if not result:
        msg = 'Buildname does not match the pattern %s'
        raise ValueError(msg % buildname)
    return result


def cmp_builds(b1, b2):
    """Compare two build names like R25-3122.0.0-rc4. For sorting."""
    b1 = build2tuple(b1)
    b2 = build2tuple(b2)
    return cmp(b1, b2)


def html_hyperlink(url, display):
    """Simplest creation of an html hyperlink."""
    return '<a href="%s">%s</a>' % (url, display)


def issue_url(description, template='Test Bug Report', summary=None,
                    labels=None):
    """Populate a hyperlink to report an issue (bug/feature).

    Filing bugs entails a lot of copying from test results. This makes it
    easy by copying the content for the triage engineer.

    Need to know the query parameters for Issues from:
      https://code.google.com/p/support/wiki/IssueTracker#Issue_entry

    Args:
        description: (comment) the contents of the bug/issue.
        template: common options include: 'Test Bug Report',
                  'Defect report from developer', 'New Feature'.
        summary: if desired to pre-populate a short summary.
        labels: if desired to pre-populate labels.
    Returns:
        String of the html hyperlink.
    """
    template = 'template=%s' % template
    comment = ('comment=[Remember: create bugs with your @chromium.org '
               'account so proper labels are included]%%0A%%0A%s' % description)
    bug_url = '%s/entry?%s&%s' % (BUG_URL_BASE, template, comment)
    if summary:
        bug_url += '&summary=%s' % summary
    if labels:
        bug_url += '&labels=%s' % labels
    return bug_url


def natsort_key(s):
    """Convert a string to a tuple with all numeric components as ints."""
    tpl = [natsort_try_int(x) for x in NATSORT_REGEXP.findall(s)]
    return tpl


def natsort_add_zeros(s, digits=6):
    """Prepend all numbers in a string with zeros.

    Good for making strings with numbers sort properly.
    Example:
        pic9.jpg  -> pic000009.jpg
        pic10.jpg -> pic000010.jpg
    """
    template = '0%dd' % digits
    template = '%' + template
    tpl = natsort_key(s)
    strings = [template % x if type(x) is int else x  for x in tpl]
    return ''.join(strings)


def string2bool(bool_str):
    """Gracefully handle unexpected bool query parameter values."""
    bool_str = bool_str.strip().lower()
    if bool_str in ['false', 'true']:
        return bool_str == 'true'
    msg = 'Expected bool instead of %s' % bool_str
    raise whining_errors.WhiningInputError(msg)


def string2int(int_str):
    """Gracefully handle unexpected int query parameter values."""
    try:
        return int(int_str)
    except ValueError:
        msg = 'Expected int instead of %s' % int_str
        raise whining_errors.WhiningInputError(msg)


def squeeze_build(build):
    """Modify (long) sortable build into a viewer friendly build.

    Sortable build: R00026-03635.00000.00000
    Viewer friends build: R26-3635.0.0

    Expects the build to use the format RWW-X.Y.Z
    The opposite of build2sortable().

    Args:
        build: sortable build.

    Returns:
        viewer friendly build withe extra 0's removed.
    """
    temp = build.split('-')
    parts = [temp[0][1:]]
    parts += temp[1].split('.')
    parts = [p.lstrip('0') for p in parts]
    parts = [p if p else '0' for p in parts]
    return 'R%s-%s' % (parts[0], '.'.join(parts[1:]))


def natsort_try_int(s):
    """Helper to safely handle int conversion."""
    try:
        return int(s)
    except ValueError:
        return s


def utc2unix(dt_utc):
    """Convert a datetime object with UTC time to unix timstamp.

    Similar to calendar.timegm() but takes a datetime object as input and
    preserves sub-second accuracy.
    See http://en.wikipedia.org/wiki/Unix_time
    """
    delta = dt_utc - EPOCH
    time_unix = delta.days * 24 * 3600
    time_unix += delta.seconds + delta.microseconds / 1000000.
    return time_unix


def format_rate(rate):
    """
    Format a number in interval [0-1] as percents with good looking precision.

    Examples:
        0.02%, 0.39%, 4.8%, 24%, 100%
        0 (strict zero), 0.00% (not zero but less than 5*10^-5)

    """
    if rate == 0:
        return  '0'
    elif rate < 0.01:
        return '{:0.2%}'.format(rate)
    elif rate < 0.1:
        return '{:0.1%}'.format(rate)
    else:
        return '{:0.0%}'.format(rate)


def logcolor(rate, scale=2.2):
    """Calculate a color from logarithmic red-green heatmap.

    Green for rate near 0, red for rate near 1.

    @param scale: Scale is roughly the number of zeros after decimal point
            needed to get the greenest green.

    """
    lograte = (math.log10(max(rate, 10 ** -scale)) + scale) / scale
    red = int(round(lograte * 127))
    rgb = (128 + red, 255 - red, 128)
    color = '#%02x%02x%02x' % rgb
    return color


def normalize_reason(reason):
    """
    Used for grouping similar reason strings together.

    In most cases, reason strings arising from the same problem differ only in
    numbers (such as timestamps) or some additional data listed towards the end
    of the string. This "normalized" version of the reason string allows to
    group such reason strings together.

    """
    short = reason[:REASON_NORMALIZATION_LENGTH].strip()
    without_numbers = NUMBER_REGEXP.sub('', short)
    return without_numbers


def make_dict_cleaner(fields):
    """
    Returns a convenience function for making modified copies of dicts.

    The returned function accepts a dict and optional additional fields to copy
    from the original dict, renaming them on the fly if requested.

    Example:
        clean = make_dict_cleaner(['test_name', 'platform'])
        d = clean(row, 'is_experimental', pass_count='cnt')

        This is equivalent to:
        d = {}
        d['test_name'] = row['test_name']
        d['platform'] = row['platform']
        d['is_experimental'] = row['is_experimental']
        d['pass_count'] = row['cnt']

    """
    def clean_dict(d, *extra_lst, **extra_named):
        new_dict = dict((k, d[k]) for k in fields if k in d)
        for k in extra_lst:
            if k in d:
                new_dict[k] = d[k]
        for (new_k, k) in extra_named.items():
            if k in d:
                new_dict[new_k] = d[k]
        return new_dict
    return clean_dict


class MinMax(object):
    """An object that keeps min max and count of all things added to it"""
    def __init__(self):
        self.min = None
        self.max = None
        self.count = 0

    def add(self, x):
        self.count += 1
        if self.min is None or x < self.min:
            self.min = x
        if self.max is None or x > self.max:
            self.max = x


class Timer(object):
    """Simple timer to measure how long stuff takes.

    Mostly for timing DB queries for dev purposes.
    """
    def __init__(self):
        self._checkpoints = {}
        self.timings = {}

    def start(self, name):
        self._checkpoints[name] = time.time()

    def stop(self, name):
        t = time.time() - self._checkpoints.pop(name)
        self.timings[name] = t * 1000
        msg = "Timed {time:.3f} ms for {name}"
        logging.info(msg.format(time=t, name=name))
        return t

    def __str__(self):
        lst = ['%s=%0.1f' % item for item in self.timings.items()]
        s = ', '.join(lst)
        return 'Timings [ms] ' + s
