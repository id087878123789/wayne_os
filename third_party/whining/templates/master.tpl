%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%from src import settings
%_root = settings.settings.relative_root
<!DOCTYPE html>
%if 'angular' in dir() and angular:
<html class="google" lang="en" ng-app="whining_app">
%else:
<html class="google" lang="en">
%end
  <head>
    <meta charset="utf-8">
    <meta content="initial-scale=1, minimum-scale=1, width=device-width" name="viewport">
    <link rel="stylesheet" href="{{ _root }}/static/css/default.css" type="text/css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="//www.google.com/js/google.js"></script>
    <script src="{{ _root }}/static/js/whining.js"></script>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;lang=en" rel="stylesheet">
    <link href="//www.google.com/css/maia.css" rel="stylesheet">
    %if 'angular' in dir() and angular:
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.4/angular.min.js"></script>
    <script>
      var app = angular.module('whining_app', []);
      app.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');
      });
    </script>
    %end
    <title>{{title or 'No title'}} - WMatrix</title>
    %include('head.tpl')
  </head>

  <body class="interface" onload="start()">
    %_embedded = [(k, v) for k, v in query_string(list_qps=True)
    %                    if k.lower() == 'embedded']

    %if not _embedded:
% import time
<div class="timings">
  Generated: {{ time.strftime('%b %d, %H:%M:%S UTC', time.gmtime()) }}
</div>

      %include('header.tpl')
    %end

    %include('psa.tpl')

    %# Not using maia-main because it maintains too narrow tables.
    %#<div id="maia-main" role="main">
    %body_block()
    %#</div>

    %if not _embedded:
      %include('footer.tpl')
    %end

    <div>
    </div>
  </body>
</html>
