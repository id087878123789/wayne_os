%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='suites')

  %# --------------------------------------------------------------------------
  %# Suites
  %if not tpl_vars.get('suites'):
    <h3><u>No suites found.</u></h3>
  %else:
    <div id="divSuites" class="lefted">
      <table class="alternate_background">
        <tbody>
          <tr>
            <th class="headeritem centered">#</th>
            <th class="headeritem lefted">
              Suite Name - {{ len(tpl_vars['suites']) }} total suites
            </th>
          </tr>
          %i = 0
          %for suite in tpl_vars['suites']:
            <tr>
              <td class="centered">{{ i }}</td>
              <td class="lefted">{{! suite }}</td>
            </tr>
            %i += 1
          %end
        </tbody>
      </table>
    </div>
  %end
%end

%rebase('master.tpl', title='suites', query_string=query_string, body_block=body_block)
