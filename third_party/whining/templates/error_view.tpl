%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%def body_block():
  <h2>Chromium OS Test Results (Whining) - Errors Encountered!</h2>
  {{ tpl_vars['exception_message'] }}
%end

%rebase('master.tpl', title='Error', query_string=query_string, body_block=body_block)
