%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root


%def body_block():
  <table>
  <tbody>
  <tr>
    <td class="headeritem lefted">
      Quick links
    </td>
    <td class="headeritem lefted">
      Stats &amp; lookups
    </td>
    <td class="headeritem lefted">
      Usage tips
    </td>
  </tr>
  <tr>
    <td class="lefted">
      <ul>
        % include urlhelp_extra_quicklinks
        <li>
          <a href="{{ _root }}/bvt?hide_experimental=True">BVT</a> w/o experimental
        </li>
        <li><a href="{{ _root }}/suites">Suite list</a></li>
        <li><a href="{{ _root }}/tests">Test list</a></li>
      </ul>
      Named filters:
      <ul>
        %for _f in tpl_vars['filter_names']:
          %if _f in ['unfiltered', 'lookups', 'browsertests', 'health']:
            % continue
          %end
        <li><a href="{{ _root }}/{{ _f }}">{{ _f }}</a></li>
        %end
      </ul>
    </td>
    <td class="lefted">
      <ul>
        <li><a href="{{ _root }}/filters">Named filters</a> - with definitions</li>
        <li>
            <a href="{{ _root }}/tempfilter">Filter wizard</a>
            - for constructing custom filters
        </li>

        <li>
            <a href="{{ _root }}/suitehealth">Suite health</a>
            - per-suite failure rates
        </li>

        <li><a href="{{ _root }}/testhealth/unfiltered/?days_back=7">Tests</a>
            - sorted by failure rate
        </li>

        <li><a href="{{ _root }}/builds">Builds</a></li>
        <li><a href="{{ _root }}/platforms">Platforms</a></li>
        <li><a href="{{ _root }}/releases">Releases</a></li>
        <li><a href="{{ _root }}/suitestats">Suite stats</a></li>
        <li> Retry stats
          <ul>
            <li><a href="{{ _root }}/retry_teststats/?days_back=30">Test stats</a></li>
            <li><a href="{{ _root }}/retry_hoststats/?days_back=30">Host stats</a></li>
          </ul>
        </li>

      </ul>
    </td>
    <td class="lefted">
      General tips:
      <ul>
        <li>
          Look at the the URLs - query parameters are designed to be human
          readable.
        </li>
        <li>
          All times/dates shown are in UTC.
        </li>
        <li>
          By default most views will show data for the last 7-12 builds. You
          can change this using the days_back query parameter.
        </li>
      </ul>

      % include urlhelp_extra

      Custom Views:
      <ul>
        <li>May be constructed using the <a href="{{ _root }}/tempfilter">{{ _root }}/tempfilter</a>
            view and saved in bookmarks.
        </li>
        <li>The following Query Parameters are available:<br>
          {{ tpl_vars['known_qvars'] }}.
        </li>
        <li>
          The builds and tests query parameters support a subset of regular
          expression syntax, see examples below.
          Allowed characters are: |, ., ?, *, [, ], ^, $.
        </li>
      </ul>

      Common examples include:
      <ul>
      <li>?days_back=14
      <li>?hide_experimental=True
      <li>?hide_missing=True
      <li>?releases=30
      <li>?releases=30,29
      <li>?suites=bvt
      <li>?releases=30&suites=hwqual
      <li>?tests=power_Resume
      <li>?tests=security_.*
      <li>?builds=R31-4601.0.0
      <li>?builds=R31-46.*
      <li>?builds=R31-46[012].* (regexp syntax)
      </ul>

    </td>
  </tr>
  </tbody>
  </table>

%end

%rebase('master.tpl', title='Help', query_string=query_string, body_block=body_block)
