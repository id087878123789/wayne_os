commit 04b33e21866412689f18b7ad6daf0a54d8f959a7
Author: Khem Raj <raj.khem@gmail.com>
Date:   Wed Jun 28 13:44:52 2017 -0700

    Replace 'struct ucontext' with 'ucontext_t' type
    
    glibc used to have:
    
       typedef struct ucontext { ... } ucontext_t;
    
    glibc now has:
    
       typedef struct ucontext_t { ... } ucontext_t;
    
    (See https://sourceware.org/bugzilla/show_bug.cgi?id=21457
     for detail and rationale for the glibc change)
    
    However, QEMU used "struct ucontext" in declarations. This is a
    private name and compatibility cannot be guaranteed. Switch to
    only using the standardized type name.
    
    Signed-off-by: Khem Raj <raj.khem@gmail.com>
    Message-id: 20170628204452.41230-1-raj.khem@gmail.com
    Cc: Kamil Rytarowski <kamil@netbsd.org>
    Cc: Riku Voipio <riku.voipio@iki.fi>
    Cc: Laurent Vivier <laurent@vivier.eu>
    Cc: Paolo Bonzini <pbonzini@redhat.com>
    Reviewed-by: Eric Blake <eblake@redhat.com>
    [PMM: Rewrote commit message, based mostly on the one from
     Nathaniel McCallum]
    Signed-off-by: Peter Maydell <peter.maydell@linaro.org>

diff -ur qemu-2.6.0/linux-user/signal.c qemu-2.6.0/linux-user/signal.c
--- qemu-2.6.0/linux-user/signal.c
+++ qemu-2.6.0/linux-user/signal.c
@@ -3019,7 +3019,7 @@
     *
     *   a0 = signal number
     *   a1 = pointer to siginfo_t
-    *   a2 = pointer to struct ucontext
+    *   a2 = pointer to ucontext_t
     *
     * $25 and PC point to the signal handler, $29 points to the
     * struct sigframe.
@@ -3404,7 +3404,7 @@
 
 struct rt_signal_frame {
     siginfo_t info;
-    struct ucontext uc;
+    ucontext_t uc;
     uint32_t tramp[2];
 };
 
@@ -3618,7 +3618,7 @@
         siginfo_t *pinfo;
         void *puc;
         siginfo_t info;
-        struct ucontext uc;
+        ucontext_t uc;
         uint16_t retcode[4];      /* Trampoline code. */
 };
 
@@ -3919,7 +3919,7 @@
         tswap_siginfo(&frame->info, info);
     }
 
-    /*err |= __clear_user(&frame->uc, offsetof(struct ucontext, uc_mcontext));*/
+    /*err |= __clear_user(&frame->uc, offsetof(ucontext_t, uc_mcontext));*/
     __put_user(0, &frame->uc.tuc_flags);
     __put_user(0, &frame->uc.tuc_link);
     __put_user(target_sigaltstack_used.ss_sp,
@@ -4393,7 +4393,7 @@
 
 struct target_ucontext {
     target_ulong tuc_flags;
-    target_ulong tuc_link;    /* struct ucontext __user * */
+    target_ulong tuc_link;    /* ucontext_t __user * */
     struct target_sigaltstack tuc_stack;
 #if !defined(TARGET_PPC64)
     int32_t tuc_pad[7];
diff -ur qemu-2.6.0/tests/tcg/test-i386.c qemu-2.6.0/tests/tcg/test-i386.c
--- qemu-2.6.0/tests/tcg/test-i386.c
+++ qemu-2.6.0/tests/tcg/test-i386.c
@@ -1720,7 +1720,7 @@
 
 void sig_handler(int sig, siginfo_t *info, void *puc)
 {
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 
     printf("si_signo=%d si_errno=%d si_code=%d",
            info->si_signo, info->si_errno, info->si_code);
@@ -1912,7 +1912,7 @@
 /* specific precise single step test */
 void sig_trap_handler(int sig, siginfo_t *info, void *puc)
 {
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     printf("EIP=" FMTLX "\n", (long)uc->uc_mcontext.gregs[REG_EIP]);
 }
 
diff -ur qemu-2.6.0/user-exec.c qemu-2.6.0/user-exec.c
--- qemu-2.6.0/user-exec.c
+++ qemu-2.6.0/user-exec.c
@@ -57,7 +57,7 @@
 void cpu_resume_from_signal(CPUState *cpu, void *puc)
 {
 #ifdef __linux__
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 #elif defined(__OpenBSD__)
     struct sigcontext *uc = puc;
 #endif
@@ -171,7 +171,7 @@
 #elif defined(__OpenBSD__)
     struct sigcontext *uc = puc;
 #else
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 #endif
     unsigned long pc;
     int trapno;
@@ -226,7 +226,7 @@
 #elif defined(__OpenBSD__)
     struct sigcontext *uc = puc;
 #else
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 #endif
 
     pc = PC_sig(uc);
@@ -288,7 +288,7 @@
 
 #ifdef __APPLE__
 #include <sys/ucontext.h>
-typedef struct ucontext SIGCONTEXT;
+typedef ucontext_t SIGCONTEXT;
 /* All Registers access - only for local access */
 #define REG_sig(reg_name, context)              \
     ((context)->uc_mcontext->ss.reg_name)
@@ -331,7 +331,7 @@
 #if defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
     ucontext_t *uc = puc;
 #else
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 #endif
     unsigned long pc;
     int is_write;
@@ -358,7 +358,7 @@
                            void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     uint32_t *pc = uc->uc_mcontext.sc_pc;
     uint32_t insn = *pc;
     int is_write = 0;
@@ -456,7 +456,7 @@
 #if defined(__NetBSD__)
     ucontext_t *uc = puc;
 #else
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
 #endif
     unsigned long pc;
     int is_write;
@@ -483,7 +483,7 @@
 int cpu_signal_handler(int host_signum, void *pinfo, void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     uintptr_t pc = uc->uc_mcontext.pc;
     uint32_t insn = *(uint32_t *)pc;
     bool is_write;
@@ -512,7 +512,7 @@
                        void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     unsigned long pc;
     int is_write;
 
@@ -534,7 +534,7 @@
 int cpu_signal_handler(int host_signum, void *pinfo, void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     unsigned long ip;
     int is_write = 0;
 
@@ -565,7 +565,7 @@
                        void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     unsigned long pc;
     uint16_t *pinsn;
     int is_write = 0;
@@ -618,7 +618,7 @@
                        void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     greg_t pc = uc->uc_mcontext.pc;
     int is_write;
 
@@ -634,7 +634,7 @@
                        void *puc)
 {
     siginfo_t *info = pinfo;
-    struct ucontext *uc = puc;
+    ucontext_t *uc = puc;
     unsigned long pc = uc->uc_mcontext.sc_iaoq[0];
     uint32_t insn = *(uint32_t *)pc;
     int is_write = 0;
