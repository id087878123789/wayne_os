From: Caroline Tice <cmtice@google.com>
Date: 10-Sep-2018

This patch reverts LLVM commit r335145.  It is one of two commits that were
identified in https://buganizer.corp.google.com/issues/113573336 as responsible
for crashes with LLVM r339409.  The actual commit that this reverts is here:
 https://reviews.llvm.org/D47898.

This patch is intended to be temporary:  Once the bug is fixed this patch can
be removed.

diff --git a/lib/Linker/IRMover.cpp b/lib/Linker/IRMover.cpp
index 738dec8e1f2..6dc264a0757 100644
--- a/llvm/lib/Linker/IRMover.cpp
+++ b/llvm/lib/Linker/IRMover.cpp
@@ -240,27 +240,18 @@ Type *TypeMapTy::get(Type *Ty, SmallPtrSet<StructType *, 8> &Visited) {
   // These are types that LLVM itself will unique.
   bool IsUniqued = !isa<StructType>(Ty) || cast<StructType>(Ty)->isLiteral();
 
-  if (!IsUniqued) {
-    StructType *STy = cast<StructType>(Ty);
-    // This is actually a type from the destination module, this can be reached
-    // when this type is loaded in another module, added to DstStructTypesSet,
-    // and then we reach the same type in another module where it has not been
-    // added to MappedTypes. (PR37684)
-    if (STy->getContext().isODRUniquingDebugTypes() && !STy->isOpaque() &&
-        DstStructTypesSet.hasType(STy))
-      return *Entry = STy;
-
 #ifndef NDEBUG
+  if (!IsUniqued) {
     for (auto &Pair : MappedTypes) {
       assert(!(Pair.first != Ty && Pair.second == Ty) &&
              "mapping to a source type");
     }
+  }
 #endif
 
-    if (!Visited.insert(STy).second) {
-      StructType *DTy = StructType::create(Ty->getContext());
-      return *Entry = DTy;
-    }
+  if (!IsUniqued && !Visited.insert(cast<StructType>(Ty)).second) {
+    StructType *DTy = StructType::create(Ty->getContext());
+    return *Entry = DTy;
   }
 
   // If this is not a recursive type, then just map all of the elements and
