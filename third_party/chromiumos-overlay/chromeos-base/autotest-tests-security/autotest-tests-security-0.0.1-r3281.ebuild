# Copyright 2014 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=5
CROS_WORKON_COMMIT="cdee4f6aea0f69056dd9fc0e99f0fb51e521a2d3"
CROS_WORKON_TREE="aafee2cd51600c9496d5cc26a137b139a36663ea"
CROS_WORKON_PROJECT="chromiumos/third_party/autotest"
CROS_WORKON_LOCALNAME=../third_party/autotest/files

inherit cros-workon autotest libchrome

DESCRIPTION="Security autotests"
HOMEPAGE="http://www.chromium.org/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="*"
# Enable autotest by default.
IUSE="+autotest -chromeless_tests -chromeless_tty containers +seccomp selinux"

RDEPEND="
	!<chromeos-base/autotest-tests-0.0.3
	tests_security_Minijail0? ( sys-apps/keyutils )
	containers? (
		tests_security_Libcontainer? (
			chromeos-base/minijail
			chromeos-base/libcontainer
		)
	)
"
DEPEND="${RDEPEND}"

IUSE_TESTS="
	!chromeless_tty? (
		!chromeless_tests? (
			+tests_security_RendererSandbox
			+tests_security_SessionManagerDbusEndpoints
		)
	)
	seccomp? (
		+tests_security_Minijail_seccomp
		+tests_security_SeccompSyscallFilters
	)
	+tests_security_AltSyscall
	+tests_security_ASLR
	+tests_security_CpuVulnerabilities
	containers? (
		+tests_security_DeviceJail_AllowDeny
		+tests_security_DeviceJail_Detach
		+tests_security_DeviceJail_Filesystem
		+tests_security_DeviceJail_Lockdown
	)
	containers? ( +tests_security_Libcontainer )
	+tests_security_Minijail0
	+tests_security_NosymfollowMountOption
	+tests_security_OpenFDs
	+tests_security_ProcessManagementPolicy
	+tests_security_ptraceRestrictions
	+tests_security_RootCA
	+tests_security_RootfsOwners
	containers? ( +tests_security_RunOci )
	+tests_security_RuntimeExecStack
	+tests_security_SandboxedServices
	selinux? ( +tests_security_SELinux )
	+tests_security_SysVIPC
	x86? ( +tests_security_x86Registers )
	amd64? ( +tests_security_x86Registers )
"

IUSE="${IUSE} ${IUSE_TESTS}"

AUTOTEST_FILE_MASK="*.a *.tar.bz2 *.tbz2 *.tgz *.tar.gz"
