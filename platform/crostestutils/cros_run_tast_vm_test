#!/bin/bash

# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Starts a VM and runs one or more Tast tests inside of it.
#
# See https://chromium.googlesource.com/chromiumos/platform/tast/ for details.

# This can only run outside the chroot.
. $(dirname "$(readlink -f "$0")")/outside_chroot_common.sh || exit 1
. "${SCRIPT_ROOT}/common.sh" || exit 1
. "${SCRIPT_ROOT}/lib/cros_vm_lib.sh" || die "Unable to load cros_vm_lib.sh"
. "${SCRIPT_ROOT}/lib/cros_vm_constants.sh" ||
  die "Unable to load cros_vm_constants.sh"

FLAGS_HELP=$(cat <<-EOF
Usage: $0 [flags] <test-expr> <test-expr> ...
Starts a VM and runs one or more Tast tests inside of it.
See https://goo.gl/UPNEgT for info about test expressions.
EOF
)

# Note that cros_vm_lib.sh defines a bunch of additional flags.
DEFINE_string board "$DEFAULT_BOARD" "Board used to run tests." b
DEFINE_string image_path "" "Full path to the VM image."
DEFINE_string results_dir "" "Results directory (within the chroot)."

set -e

# Parse command line.
FLAGS "$@" || exit 1
eval set -- "${FLAGS_ARGV}"

[ -n "${FLAGS_board}" ] || die "Board must be specified (-b or --board)."

if [ -z "${FLAGS_image_path}" ]; then
  LATEST_IMAGE="$("${SCRIPT_ROOT}/get_latest_image.sh" \
      --board=${FLAGS_board})/${DEFAULT_QEMU_IMAGE}"
  info "Using latest vm image ${LATEST_IMAGE}"
  FLAGS_image_path=${LATEST_IMAGE}
fi

[ -e "${FLAGS_image_path}" ] || die "Image ${FLAGS_image_path} does not exist."

run_args=( )
if [ -n "${FLAGS_results_dir}" ]; then
  run_args+=("-resultsdir=${FLAGS_results_dir}")
fi
if [ -n "${FLAGS_ssh_private_key}" ]; then
  chroot_key="$(reinterpret_path_for_chroot "${FLAGS_ssh_private_key}")"
  run_args+=("-keyfile=${chroot_key}")
fi

# Inject a "tast_vm" USE flag to be used when computing dependencies.
# This makes it possible to skip tests that require real hardware.
run_args+=("-extrauseflags=tast_vm")

trap stop_kvm EXIT
start_kvm "${FLAGS_image_path}" "${FLAGS_board}"
retry_until_ssh

${GCLIENT_ROOT}/chromite/bin/cros_sdk -- \
    tast -verbose run -build=false "${run_args[@]}" \
    127.0.0.1:${FLAGS_ssh_port} "$@"
