// Copyright 2018 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package subtest contains subtests executed by the vm.CrostiniStartEverything test.
//
// Crostini initialization can be time-consuming, so this approach allows setup to be performed just once.
package subtest
